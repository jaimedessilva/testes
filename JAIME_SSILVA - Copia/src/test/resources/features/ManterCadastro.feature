#language: pt
#enconding: UTF-8

@manterCadastro
Funcionalidade: Login

@cadastrarUsuario
Cenario: Cadastrar Usuario
Quando acionar a aba Admin
E acionar o botao Add
E informar o campo Employee name "Fiona Grace"
E informar no campo username "usuario123"
E informar o campo password "123456789"
E informar o campo confirm password "123456789"
E acionar o botao EditSalvar 

@editarUsuario
Cenario: editar Usuario
Quando acionar a aba Admin
E clicar no usuario "Usuario10"
E acionar o botao EditSalvar
E informar no campo username "Usuario11"
E acionar o botao EditSalvar
Entao o sistema cadastra o usuario "Usuario11"

@mouseHover
Cenario: Acessar custom fields
Quando clicar mo menu custom fields
Entao o sistema apresenta a tela de add custom field

@PesquisarUsuario
Cenario: Pesquisar Usuario
Quando acionar a aba Admin
E selecionar o User Role "Admin"
E acionar o botao Search
Entao o sistema realiza a pesquisa com sucesso


@login @Teste2
Cenario: Clicar
Quando acionar a aba Admin
E clicar no usuario "Admin"
E acionar o botao EditSalvar
E informar o campo Employee name "Roberta Miranda"
E informar no campo username "roberta.miranda"
E clicar mo menu change password
E informar o campo password "123456789"
E informar o campo confirm password "123456789"
E acionar o botao EditSalvar
