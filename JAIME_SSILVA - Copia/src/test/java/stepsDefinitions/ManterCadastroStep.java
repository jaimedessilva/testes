package stepsDefinitions;

import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pageObjects.ManterUsuario;

/** Project: automacao-teste
 *  File: ManterCadastroStep.java
 *  @author jaime Des
 *  Date: 28 de jul de 2020 **/

public class ManterCadastroStep extends ManterUsuario {
	
	//Clicar Admin
	@Quando("acionar a aba Admin")
	public void acionar_a_aba_Admin() {
	    clicarAbaAdmin();
	}
	
	@Quando("clicar no usuario {string}")
	public void clicar_no_usuario(String nome) {
	   clicarUsuario(nome);
		
	}
	
	@Quando("acionar o botao EditSalvar")
	public void acionar_o_botao_EditSalvar() throws InterruptedException {
	    clickEdit();
	}
	@Quando("informar o campo Employee name {string}")
	public void informar_o_campo_Employee_name(String nome) {
	   updateNome(nome);
	}
	
	@Quando("informar no campo username {string}")
	public void informar_no_campo_username(String username) {
	    updateUsername(username);
	}
	
	@Quando("clicar mo menu change password")
	public void clicarChangePass () {
		chkPass ();
	}
	@Quando("clicar mo menu custom fields")
	public void clicar_mo_menu_custom_fields() {
		acessarSubMenuCustomField();
	}
	
	@Quando("acionar o botao Add")
	public void acionar_o_botao_Add() {
	    clickAdd();
	}

	@Quando("informar o campo password {string}")
	public void informar_o_campo_password(String pass) {
		setPassword(pass);
	}

	@Quando("informar o campo confirm password {string}")
	public void informar_o_campo_confirm_password(String cpass) {
		setConfirmPass (cpass);
	}

	@Entao("o sistema cadastra o usuario {string}")
	public void o_sistema_cadastra_o_usuario(String string) {
	   
	}
	@Entao("o sistema apresenta a tela de add custom field")
	public void o_sistema_apresenta_a_tela_de_add_custom_field() {
	   
	}

	@Quando("selecionar o User Role {string}")
	public void selecionar_o_User_Role(String string) {
		setUserRole();
	}

	@Quando("acionar o botao Search")
	public void acionar_o_botao_Search() {
	    clickSearch();
	}

	@Entao("o sistema realiza a pesquisa com sucesso")
	public void o_sistema_realiza_a_pesquisa_com_sucesso() {
		clickSearch ();
	}


}
