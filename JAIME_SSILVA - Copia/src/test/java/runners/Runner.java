package runners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

/** Project: automacao-teste
 *  File: Runner.java
 *  @author jaime Des
 *  Date: 27 de jul de 2020 **/

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"html:target/cucumber-report"},
		features = "src/test/resources/features/",
		glue = "stepsDefinitions",
		dryRun = false,
		tags = {"@loginSucesso"},
		monochrome = true)
public class Runner {}
