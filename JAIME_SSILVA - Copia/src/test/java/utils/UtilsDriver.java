package utils;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/** Project: automacao-teste
 *  File: UtilsDriver.java
 *  @author jaime Des
 *  Date: 27 de jul de 2020 **/

public class UtilsDriver {

	
	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.home")+"/driver/chromedriver");
		
		//Instancia Driver
		WebDriver driver = new ChromeDriver(); 
		//Maximizar Janela
		driver.manage().window().maximize();
		//Espera Implicita de 5 segundos
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//Url a navegar
		driver.get("https://app-sgveiculos.herokuapp.com/login");
		
		
		
		
		//user
		WebElement user = driver.findElement(By.id("username"));
		user.sendKeys("user");
		
		//user
		WebElement pass = driver.findElement(By.id("password"));
		pass.sendKeys("123");
		
				
		//Click logar
		WebElement btnLogin = driver.findElement(By.xpath("/html/body/div/form/div[3]/button"));
		btnLogin.click();
		
		//Click admin
		WebElement btnAdmin = driver.findElement(By.xpath("//*[@id=\"wrapper\"]/nav/div[2]/ul/li[1]/a"));
		btnAdmin.click();
		
		//Click Cad Us
		WebElement btnCadUs = driver.findElement(By.xpath("//*[@id=\"submenu-1\"]/li[2]/a"));
		btnCadUs.click();
		
		
		
		
	}

}
