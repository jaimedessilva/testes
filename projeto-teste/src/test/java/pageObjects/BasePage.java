package pageObjects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static utils.Utils.driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/** Project: projeto-teste
 *  File: BasePage.java
 *  @author jaime Des
 *  Date: 27 de jul de 2020 **/

public class BasePage {
	
	/*
	 * Construct
	 */
	public BasePage() {
		PageFactory.initElements(driver, this);
	}
	/*
	 * Web Elements
	 */
	@FindBy (id = "welcome")
	private WebElement textoUsuarioLogado;
	
	@FindBy (xpath= "//span[@id='notification']/.") // xpath
	private WebElement iconSino;
	
	@FindBy (id ="notification") //id
	private WebElement iconN;
	
	@FindBy (xpath = "//*[@id=\"menu_admin_viewAdminModule\"]/b")
	private WebElement abaAdmin;

	/*
	 *  Métodos
	 */
	public void usuarioLogado (String logado) {
		assertEquals(logado, textoUsuarioLogado.getText());
//		System.out.println("O resultado esperado é: " + logado);
//		System.out.println("O resultado é: " + textoUsuarioLogado.getText());
	}
	public void validIcone () {
		assertTrue(iconSino.isDisplayed());
		System.out.println("Esse é o retorno: " + iconSino.isDisplayed());
	}
//	public void validIcone1 () {
//      validacao por id	
//		assertTrue(iconN.isDisplayed());
//		System.out.println("Esse é o retorno: " + iconN.isDisplayed());
//	}
	public void clicarAbaAdmin () {
		abaAdmin.click();
	}

}
