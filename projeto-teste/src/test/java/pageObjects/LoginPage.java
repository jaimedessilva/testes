package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertEquals;
import static utils.Utils.driver; //Inport estático

/** Project: projeto-teste
 *  File: LoginPage.java
 *  @author jaime Des
 *  Date: 27 de jul de 2020 **/

public class LoginPage {
	
	/*
	 * Construct
	 */
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	/*
	 * Web Elementos HTML By ID
	 */
	@FindBy(id = "txtUsername")
	private WebElement userName;

	@FindBy(id = "txtPassword")
	private WebElement inputPass;

	@FindBy(id = "btnLogin")
	private WebElement btnLogin;

	@FindBy(linkText = "Forgot your password?")
	private WebElement linkText;
	
	@FindBy(id="spanMessage")
	private WebElement msgLogin;
	
	/*
	 * Metodos
	 */
	public void setUser(String user) {
		userName.sendKeys(user);
		
	}

	public void setPassword(String pass) {
		inputPass.sendKeys(pass);
	}

	public void clickButton() {
		btnLogin.click();
	}
	public void clickForgot () {
		linkText.click();
	}
	public void getMsg (String msg) {
		assertEquals(msg, msgLogin.getText());
		System.out.println("O Valor que eu espero é: " + msg);
		System.out.println("O Valor retornado é: " + msgLogin.getText());
	}
	public void logarSistema () {
		setUser("Admin");
		setPassword ("admin123");
		clickButton();
		
	}
}
