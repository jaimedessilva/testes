package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import static utils.Utils.driver;

/** Project: projeto-teste
 *  File: TelaUsuarioPage.java
 *  @author jaime Des
 *  Date: 27 de jul de 2020 **/

public class TelaUsuarioPage {
	/*
	 *  Construct
	 */
	public TelaUsuarioPage() {
		PageFactory.initElements(driver, this);
	}
	/*
	 *  Methods
	 */
	 public void clicarNoUsuario (String nomeUsuario) {
//		WebElement usuario = driver.findElement(By.xpath("//*[@id=\"resultTable\"]/tbody/tr[10]/td[2]/a"));
		WebElement usuario = driver.findElement(By.linkText(nomeUsuario));
		usuario.click();
	 }
	
	

}
