package utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

/** Project: projeto-teste
 *  File: Utils.java
 *  @author jaime Des
 *  Date: 25 de jul de 2020 **/

public class Utils {
	//Driver
	public static ChromeDriver driver;
	
	//Metodo para abrir o navegador
	public static void acessarSistema () {
		//Caminho do Driver
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.home")+"/driver/chromedriver");
		
		driver = new ChromeDriver(); //Instancia
		driver.manage().window().maximize(); //maximiza
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); //Espera implicita 5 seg
		
		driver.get("https://opensource-demo.orangehrmlive.com/"); // Url
		
	}

}
