package runners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;

/** Project: projeto-teste
 *  File: RunnerCucumber.java
 *  @author jaime Des
 *  Date: 25 de jul de 2020 **/

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"html:target/cucumber-report"},
		features = "src/test/resources/features/", // Caminho das Features
		glue = "stepsDefinitions", //Pasta que executa os Steps
		dryRun = false, //Gera os snipets
		tags = "@manterCadastro", // Cenario a ser executado
		snippets = SnippetType.CAMELCASE, //Formatação do Texto nos Snipets
		monochrome = true)

public class RunnerCucumber {}
