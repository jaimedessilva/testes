package stepsDefinitions;

import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pageObjects.BasePage;
import pageObjects.LoginPage;

import org.openqa.selenium.By;
import static utils.Utils.driver;

/** Project: projeto-teste
 *  File: LoginSpteps.java
 *  @author jaime Des
 *  Date: 25 de jul de 2020 **/

public class LoginSteps {
	
	LoginPage login = new LoginPage();
	BasePage basePage = new BasePage();
	
/* Dado */

//	@Dado("que eu acesso o sistema")
//	public void que_eu_acesso_o_sistema() {
//		acessarSistema();
//	}
	
	/* User */
	@Quando("eu informar o usuario {string}")
	public void euInformarOUsuario(String user) {
//	   driver.findElement(By.id("txtUsername")).sendKeys(user);
		login.setUser(user);
	}

	/* Password */
	@Quando("informar a senha {string}")
	public void informarASenha(String pass) {
//		driver.findElement(By.id("txtPassword")).sendKeys(pass);
		login.setPassword(pass);

	}

	@Quando("acionar o botao login")
	public void acionarOBotaoLogin() {
//		driver.findElement(By.id("btnLogin")).click();
		login.clickButton();

	}

	@Entao("o sistema apresenta o campo do usuario logado {string}")
	public void oSistemaApresentaOCampoDoUsuarioLogado(String msg) {
		
	}

	/* Mensagem */
	@Entao("o sistema apresenta a mensagem {string}")
	public void oSistemaApresentaAMensagem(String msg) {
		login.getMsg(msg);

	}

	@Entao("o sistema apresenta o logo do sino")
	public void oSistemaApresentaOLogoDoSino() {
		basePage.validIcone();
//		basePage.validIcone1();
	}

	/* forgot password */
	@Quando("eu clicar no forgot")
	public void euClicarNoForgot() {
//		driver.findElement(By.linkText("Forgot your password?")).click();
		login.clickForgot();
	}
	
}
