package stepsDefinitions;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import io.cucumber.java.pt.Quando;
import pageObjects.BasePage;
import pageObjects.TelaUsuarioPage;

/** Project: projeto-teste
 *  File: ManterCadastroStep.java
 *  @author jaime Des
 *  Date: 27 de jul de 2020 **/

public class ManterCadastroStep extends BasePage {
	
//	BasePage page = new BasePage();
	TelaUsuarioPage tp = new TelaUsuarioPage();
	
	
	@Quando("eu acionar a aba Admin")
	public void eu_acionar_a_aba_Admin() {
	   clicarAbaAdmin();
	}

	@Quando("clicar no usuario {string}")
	public void clicar_no_usuario(String nomeUsuario) {
	    tp.clicarNoUsuario(nomeUsuario);
	    throw new io.cucumber.java.PendingException();
	}

	@Quando("acionar o botao edit")
	public void acionar_o_botao_edit(String nomeUsuario) {
	   tp.clicarNoUsuario(nomeUsuario);
	}

	@Quando("informar no campo username {string}")
	public void informar_no_campo_username(String string) {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}
	

}
