package stepsDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import pageObjects.LoginPage;

import static utils.Utils.*; //Import Estático

/** Project: projeto-teste
 *  File: Hooks.java
 *  @author jaime Des
 *  Date: 26 de jul de 2020 **/

public class Hooks {
	/*Controle de Execução*/
	
	@Before (order = 1)
	public void suitUp () {
		//Abrir navegador
		acessarSistema();
	}
	@Before (order = 2, value= "@manterCadastro")
	public void logar () {
		LoginPage login = new LoginPage();
		login.logarSistema();
	}
	@After
	public void tearDown () throws InterruptedException {
		//Fechar o navegador
		Thread.sleep(2000);
		driver.quit();
	}
}
