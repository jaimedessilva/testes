package tests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/** Project: opensite
 *  File: OpenSiteTest.java
 *  @author Jaime Des
 *  Date: 22/07/2020 **/

public class OpenSiteTest {
	
	private WebDriver driver;
	/*
	 *  Abrir o navegador
	 */
	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.home")+"/driver/chromedriver");
		driver = new ChromeDriver();
	}
	/*
	 *  Fechar o navegador
	 */
	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() throws InterruptedException {
		driver.get("https://www.americanas.com.br/");
		assertTrue("Titulo da Pagina difere do esperado", driver.getTitle()
				.contentEquals("Americanas - Tudo. A toda hora. Em qualquer lugar."));
		Thread.sleep(3000);
	}

}
