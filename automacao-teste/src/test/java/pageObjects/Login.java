package pageObjects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static utils.Utils.driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

/** Project: automacao-teste
 *  File: Login.java
 *  @author jaime Des
 *  Date: 27 de jul de 2020 **/

public class Login {

	/**
	 * Construct
	 */
	public Login() {
		PageFactory.initElements(driver, this);
	}

	// Usuario
	public void sendUserLogin(String user) {
		driver.findElement(By.id("txtUsername")).sendKeys(user);
	}

	// Password
	public void sendPassLogin(String pass) {
		driver.findElement(By.id("txtPassword")).sendKeys(pass);

	}

	// Button
	public void clickBtnLogin() {
		driver.findElement(By.id("btnLogin")).click();
	}

	// Message
	public void getMenssage(String msg) {

		WebElement msgLogin = driver.findElement(By.id("spanMessage"));
		assertEquals(msg, msgLogin.getText());
		System.out.println("Out: " + msgLogin.getText());
	}

	// User Logged
	public void getUserLog(String logado) {

		WebElement txtLogado = driver.findElement(By.id("welcome"));
		assertEquals(logado, txtLogado.getText());
		System.out.println("Saída: " + txtLogado.getText());
	}
	//Icone Sino
	public void getLogoBell() {

		WebElement icoSino = driver.findElement(By.id("notification"));
		assertTrue(icoSino.isDisplayed());
	}
	//Logar
	public void logarSistema() {
		sendUserLogin("Admin");
		sendPassLogin("admin123");
		clickBtnLogin();

	}
	

}
