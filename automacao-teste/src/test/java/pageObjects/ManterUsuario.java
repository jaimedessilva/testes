package pageObjects;

import static utils.Utils.driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/** Project: automacao-teste
 *  File: ManterUsuario.java
 *  @author jaime Des
 *  Date: 28 de jul de 2020 **/

public class ManterUsuario {
	
	WebDriverWait wait = new WebDriverWait(driver, 10);

	/* Construct */
	public ManterUsuario() {
		PageFactory.initElements(driver, this);
	}
	/*
	 * Web Element
	 */
	@FindBy(name = "btnSave")
	WebElement botaoEditar_Salvar;
	
	@FindBy(linkText = "PIM")
	WebElement abaPIM;

	@FindBy(linkText = "Configuration")
	WebElement menuConfiguration;

	@FindBy(linkText = "Custom Fields")
	WebElement subMenuCustomField;

	// Aba Admin
	public void clicarAbaAdmin() {
		driver.findElement(By.xpath("//*[@id=\'menu_admin_viewAdminModule\']/b")).click();
	}

	// Click Name
	public void clicarUsuario(String nome) {
		driver.findElement(By.linkText(nome)).click();
	}

	// Edit
	public void clickEdit() throws InterruptedException {
		Thread.sleep(2000);
		botaoEditar_Salvar.click();
		System.out.println("passou");
	}

	// Nome New
	public void updateNome(String nome) {
		WebElement nomeNew = driver.findElement(By.id("systemUser_employeeName_empName"));
		nomeNew.clear();
		nomeNew.sendKeys(nome);
	}

	// User Name new
	public void updateUsername(String username) {
		WebElement nomeNew = driver.findElement(By.id("systemUser_userName"));
		nomeNew.clear();
		nomeNew.sendKeys(username);
		System.out.println("Passou");
	}

	public void chkPass() {
		driver.findElement(By.id("systemUser_chkChangePassword")).click();
	}

	// Pass
	public void setPassword(String password) {

		driver.findElement(By.id("systemUser_password")).sendKeys(password);
	}

	// Confirm Pass
	public void setConfirmPass(String confPass) {

		driver.findElement(By.id("systemUser_confirmPassword")).sendKeys(confPass);
	}

	public void clickAdd() {
		driver.findElement(By.id("btnAdd")).click();
	}

	public void clickSearch() {
		driver.findElement(By.id("searchBtn")).click();
	}

	public void setUserRole() {
		WebElement checkbox = driver.findElement(By.xpath("//*[@id=\"searchSystemUser_userType\"]/option[3]"));
		checkbox.click();
	}

	public void acessarSubMenuCustomField() {

//		WebElement abaPIM = driver.findElement(By.linkText("PIM"));
//		WebElement menuConfiguration = driver.findElement(By.linkText("Configuration"));
//		WebElement subMenuCustomField = driver.findElement(By.linkText("Custom Fields"));

		Actions acao = new Actions(driver);

		acao.moveToElement(abaPIM).build().perform();
		acao.moveToElement(menuConfiguration).build().perform();
		acao.moveToElement(subMenuCustomField).build().perform();
		subMenuCustomField.click();

//		acao.moveToElement(abaPIM).moveToElement(menuConfiguration).click(subMenuCustomField).build().perform();

	}

	public void esperaImplicita() {

		WebElement menuConfiguration = driver.findElement(By.linkText("Configuration"));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='notifications']")));
		menuConfiguration.click();
	}
}
