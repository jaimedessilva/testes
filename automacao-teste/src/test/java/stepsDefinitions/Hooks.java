package stepsDefinitions;

import static utils.Utils.acessarSistema;
import static utils.Utils.driver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import pageObjects.Login;

/** Project: automacao-teste
 *  File: Hooks.java
 *  @author jaime Des
 *  Date: 27 de jul de 2020 **/

public class Hooks extends Login {
	
	@Before  (order = 1) // Abrir
	public void open() {
		acessarSistema();
	}
	@Before (order = 2, value ="@manterCadastro")
	public void fazerLogin () {
		logarSistema();
	}

	@After // Fechar
	public void close() throws InterruptedException {
		Thread.sleep(4000);
		//capturarScreenshot(scenario);
		driver.quit();
	}

}
