package pageObjects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static utils.Utils.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	WebDriverWait espera = new WebDriverWait(driver, 10);
	
	public BasePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "welcome")
	private WebElement textoUsuarioLogado;
	
	@FindBy(xpath = "//span[@id='notification']/.")
	private WebElement iconeSino;
	
	@FindBy(linkText = "Admin")
	private WebElement abaAdmin;
	
	@FindBy(linkText = "PIM")
	private WebElement abaPIM;
	
	@FindBy(linkText = "Configuration")
	private WebElement menuConfiguration;
	
	@FindBy(linkText = "Custom Fields")
	private WebElement subMenuCustomField;
	
	
	
	public void validarUsuarioLogado(String string) {
		assertEquals(string, textoUsuarioLogado.getText());
	}
	
	public void validarIconeSino() {
		assertFalse(iconeSino.isSelected());
		System.out.println("Esse e o retorno: " + iconeSino.isDisplayed());
	}
	
	public void clicarAbaAdmin() {
		abaAdmin.click();
	}
	
	public void acessarSubMenuCustomField() {
		Actions acao = new Actions(driver);
		
	
		//acao.moveToElement(abaPIM).moveToElement(menuConfiguration).click(subMenuCustomField).build().perform();

		acao.moveToElement(abaPIM).build().perform();
		acao.moveToElement(menuConfiguration).build().perform();
		subMenuCustomField.click();
		
//		acao.dragAndDrop(menuConfiguration, abaPIM).build().perform();
//		acao.contextClick().build().perform();
//		
//		acao.clickAndHold();
//		acao.sendKeys(Keys.ALT);
	
	}
	
	public void esperaImplicita() {
		espera.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='notifications']")));
		menuConfiguration.click();
	}

}
