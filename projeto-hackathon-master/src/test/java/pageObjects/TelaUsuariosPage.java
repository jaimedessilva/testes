package pageObjects;

import static org.junit.Assert.assertTrue;
import static utils.Utils.driver;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class TelaUsuariosPage {
	
	//Construtor
	public TelaUsuariosPage() {
		PageFactory.initElements(driver, this);
	}
	
	//ELMENTOS
	@FindBy(xpath = "//tr//td//a")
	private List<WebElement> nomesUsuarios;
	
	@FindBy(id = "btnAdd")
	private  WebElement botaoAdd;
	
	@FindBy(name = "searchSystemUser[userType]")
	private  WebElement selectUserRole;
	
	@FindBy(name= "_search")
	private  WebElement botaoSearch;
	
	//METODOS
	public void clicarNoUsuario(String nomeUsuario) {
		WebElement usuario = driver.findElement(By.xpath("//a[.='" + nomeUsuario + "']"));
		usuario.click();
	}
	
	public void validarUsuarioCadastrado(String nomeUsuario) {
		
//	assertTrue(driver.findElement(By.xpath("//a[.='" + nomeUsuario + "']")).isDisplayed());

		boolean usuarioExiste = false;
		
		for (WebElement usuarioLista : nomesUsuarios) {
			if(usuarioLista.getText().equals(nomeUsuario)) {
				usuarioExiste = true;
				break;
			}	
			
			usuarioExiste = false;
		}
		
		assertTrue(usuarioExiste);
		
	}
	
	public void clicarNoBotaoAdd() throws Exception {
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		//js.executeAsyncScript("alert('Ol� Hackathon!!')");//
		//Thread.sleep(1000);
		//driver.switchTo().alert().accept();
		
	//	js.executeScript("document.getElementById('btnAdd').click();");//
		botaoAdd.click();
		
	}
	
	public void selecionarUserRole(String opcao) throws Exception {
		Select select = new Select(selectUserRole);
		select.selectByVisibleText(opcao);
	}
	
	
	public void acionarBotaoSearch() {
		botaoSearch.click();
		
	}

}
