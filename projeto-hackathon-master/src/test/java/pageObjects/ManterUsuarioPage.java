package pageObjects;

import static utils.Utils.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManterUsuarioPage {
	
	public ManterUsuarioPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "btnSave")
	private WebElement botaoEditar_Salvar;
	
	@FindBy(name = "systemUser[userName]")
	private WebElement campoUsername;
	
	@FindBy(name = "systemUser[employeeName][empName]")
	private WebElement campoEmplyeeName;
	
	@FindBy(id = "systemUser_password")
	private WebElement campoPassword;
	
	@FindBy(id = "systemUser_confirmPassword")
	private WebElement campoConfirmPassword;
	
	
	
	public void clicarBotaoEditar() {
		botaoEditar_Salvar.click();
	}
	
	public void escreverNoCampoUsername(String nomeUsuario) {
		campoUsername.clear();
		campoUsername.sendKeys(nomeUsuario);
	}
	
	
	public void escreverNoCampoEmployeeName(String employeeName) {
		campoEmplyeeName.sendKeys(employeeName);
	}
	
	
	public void escreverNoCampoPassword(String password) {
		campoPassword.sendKeys(password);
	}
	
	public void escreverNoConfirmPassword(String password) {
		campoConfirmPassword.sendKeys(password);
	}
	
	
}
