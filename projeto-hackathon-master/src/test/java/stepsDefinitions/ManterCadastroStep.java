package stepsDefinitions;

import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pageObjects.BasePage;
import pageObjects.ManterUsuarioPage;
import pageObjects.TelaUsuariosPage;

public class ManterCadastroStep {
	
	BasePage BasePage = new BasePage();
	TelaUsuariosPage tp = new TelaUsuariosPage();
	ManterUsuarioPage mu = new ManterUsuarioPage();
	
	@Quando("acionar a aba Admin")
	public void acionarAAbaAdmin() {
	    BasePage.clicarAbaAdmin();
	}

	@Quando("clicar no usuario {string}")
	public void clicarNoUsuario(String nomeUsuario) {
	    tp.clicarNoUsuario(nomeUsuario);
		
	}

	@E("^acionar o botao EditSalvar$")
	public void acionarOBotaoEditSalvar() throws Throwable {
		Thread.sleep(2000);
		mu.clicarBotaoEditar();
	}

	@Quando("informar no campo username {string}")
	public void informarNoCampoUsername(String nomeUsuario) {
	    mu.escreverNoCampoUsername(nomeUsuario);
	}

	@Entao("^o sistema cadastra o usuario \"([^\"]*)\"$")
	public void oSistemaCadastraOUsuario(String nomeUsuario) throws Throwable {
		tp.validarUsuarioCadastrado(nomeUsuario);
	}
	
	@E("^acionar o botao Add$")
	public void acionarOBotaoAdd() throws Throwable {
		tp.clicarNoBotaoAdd();
	}
	
	@Quando("informar o campo Employee name {string}")
	public void informarOCampoEmployeeName(String employeeName) {
		mu.escreverNoCampoEmployeeName(employeeName);
	}

	@Quando("informar o campo password {string}")
	public void informarOCampoPassword(String password) {
	   mu.escreverNoCampoPassword(password);
		
	}

	@Quando("informar o campo confirm password {string}")
	public void informarOCampoConfirmPassword(String password) {
		mu.escreverNoConfirmPassword(password);
	}

	@Quando("^selecionar o User Role \"([^\"]*)\"$")
	public void selecionarOUserRole(String opcao) throws Throwable {
		tp.selecionarUserRole(opcao);
	}

	@E("^acionar o botao Search$")
	public void acionarOBotaoSearch() throws Throwable {
		tp.acionarBotaoSearch();
	}

	@Entao("^o sistema realiza a pesquisa com sucesso$")
	public void oSistemaRealizaAPesquisaComSucesso() throws Throwable {

	}





}
