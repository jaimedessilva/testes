package stepsDefinitions;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static utils.Utils.driver;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pageObjects.ManterUsuario;

/** Project: JAIME_SSILVA
 *  File: ManterCadastroStep.java
 *  @author Jaime Des
 *  Date: 28/07/2020 **/

public class ManterCadastroStep extends ManterUsuario {
	
	WebDriverWait espera = new WebDriverWait(driver, 10);
	
	
	@Quando("acionar o botao NovoUsuario")
	public void acionar_botao_novo () {
		addNew ();
	}
	@Quando("informar o campo nome {string}")
	public void informar_o_campo_nome(String nome) {
	   setNome(nome);
	}

	@Quando("informar o campo matricula {string}")
	public void informar_o_campo_matricula(String mat) {
	   setMatricula(mat);
	}

	@Quando("clicar no campo Naj")
	public void clicar_no_campo_Naj() {
	    setNaj();
	}

	@Quando("clicar no campo cargo")
	public void clicar_no_campo_cargo() {
	    setCargo();
	}

	@Quando("informar o campo senha {string}")
	public void informar_o_campo_senha(String senha) {
	   setSenha(senha);
	}

	@Quando("informar no campo tipo de acesso")
	public void informar_no_campo_tipo_de_acesso() {
	  setTipoAc();
	}

	@Quando("clicar em cadastrar")
	public void clicar_em_cdastrar() {
		clickSave();
	}

	@Entao("o sistema cadastrar o usuario")
	public void o_sistema_cadastrar_o_usuario() {
	   //
	}

	@Quando("clicar no botao editar")
	public void clicar_no_botao_editar() throws InterruptedException {
		clickEdit();
	}
	@Quando("clicar em salvar")
	public void clicar_em_salvar() {
	    //clickSave();
	}
}
