package stepsDefinitions;

import io.cucumber.java.pt.Dado;
import static utils.Utils.acessarSistema;

/** Project: projeto-hackathon
 *  File: BaseSteps.java
 *  @author Jaime Des
 *  Date: 22/07/2020 **/

public class BaseSteps {
	
	@Dado("que eu acesse o sistema")
	public void que_eu_acesse_o_sistema() throws InterruptedException {
		acessarSistema();
	}

}
