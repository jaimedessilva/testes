package stepsDefinitions;

import static utils.Utils.acessarSistema;
import static utils.Utils.driver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import pageObjects.LoginPage;

/** Project: projeto-hackathon
 *  File: Hooks.java
 *  @author Jaime Des
 *  Date: 22/07/2020 **/

public class Hooks {
	
	@Before(order = 1)
	public void suitUp() throws InterruptedException{ //logar com usario advogado
		acessarSistema();
	}
	
	@Before(order = 2, value = "@manterCadastro")
	public void fazerLogin(){ 
		LoginPage LoginPage = new LoginPage();
		LoginPage.realizarLogin();
	}
	
	@After()
	public void apagarUsuario() throws Exception {
		driver.quit();
	}
}
