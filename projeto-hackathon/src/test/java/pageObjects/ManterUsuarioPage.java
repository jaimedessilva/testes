package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static utils.Utils.driver;

/** Project: projeto-hackathon
 *  File: ManterUsuarioPage.java
 *  @author Jaime Des
 *  Date: 24/07/2020 **/

public class ManterUsuarioPage {
	
	public ManterUsuarioPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "btnSave")
	private static WebElement botaoEditar_Salvar;
	
	@FindBy(name = "systemUser[userName]")
	private WebElement campoUsername;
	
	public static void clicarBotaoEditar() {
		botaoEditar_Salvar.click();
	}
	
	public void escreverNoCampoUsername(String nomeUsuario) {
		campoUsername.clear();
		campoUsername.sendKeys(nomeUsuario);
	}


}
