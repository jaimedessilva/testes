package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertEquals;
import static utils.Utils.driver;

/** Project: projeto-hackathon
 *  File: LoginPage.java
 *  @author jaime Des
 *  Date: 23 de jul de 2020 **/

public class LoginPage {
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "txtUsername")
	private WebElement userName;
	
	@FindBy(id = "txtPassword")
	private WebElement inputPassword;
	
	@FindBy(id = "spanMessage")
	private WebElement msgLogin;
	
	@FindBy(css = "#btnLogin")
	private WebElement btnLogin;
	
	
	public void preencherUserName(String string) {
		userName.sendKeys(string);
	}
	
	public void preencherPassword(String string) {
		inputPassword.sendKeys(string);
	}
	
	public void acionarBotaoLogin() {
		btnLogin.click();
	}
	
	public void mostrarTextorecuperado(String string) {
		assertEquals(string, msgLogin.getText());
		System.out.println("O valor esperado �: " + string);
		System.out.println("O Valor que recuperou foi: " + msgLogin.getText());
	}

	public void realizarLogin() {
		preencherUserName("Admin");
		preencherPassword("admin123");
		acionarBotaoLogin();
		
	}
	
}
